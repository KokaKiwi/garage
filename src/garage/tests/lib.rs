#[macro_use]
mod common;

mod admin;
mod bucket;
mod list;
mod multipart;
mod objects;
mod simple;
mod streaming_signature;
mod website;
